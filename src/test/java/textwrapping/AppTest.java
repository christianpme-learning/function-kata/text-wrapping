package textwrapping;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void wrapping_9_Test(){
        App app = new App();

        String text = "Es blaut die Nacht, die Sternlein blinken, Schneeflöcklein leis hernieder sinken.";

        String actual = app.wrapping(text, 9);

        String expected =   "Es blaut \n"+
                            "die      \n"+
                            "Nacht,   \n"+
                            "die      \n"+
                            "Sternlein\n"+
                            "blinken, \n"+
                            "Schneeflö\n"+
                            "cklein   \n"+
                            "leis     \n"+
                            "hernieder\n"+
                            "sinken.  ";

        assertTrue(expected.equals(actual));
    }

    @Test
    public void wrapping_14_Test(){
        App app = new App();

        String text = "Es blaut die Nacht, die Sternlein blinken, Schneeflöcklein leis hernieder sinken.";

        String actual = app.wrapping(text, 14);

        String expected =   "Es blaut die  \n"+
                            "Nacht, die    \n"+
                            "Sternlein     \n"+
                            "blinken,      \n"+
                            "Schneeflöcklei\n"+
                            "n leis        \n"+
                            "hernieder     \n"+
                            "sinken.       ";

        assertTrue(expected.equals(actual));
    }
}
