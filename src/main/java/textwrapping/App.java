package textwrapping;

public class App 
{
	public String wrapping(String text, int maxLineLength) {
        StringBuilder textBuilder = new StringBuilder();
        StringBuilder lineBuilder = new StringBuilder();
        StringBuilder wordBuilder = new StringBuilder();

        for(Character character : text.toCharArray()){

            if(Character.isWhitespace(character)){

                if(!isOneWordLineSpace(wordBuilder, maxLineLength))
                    wordBuilder.append(character);

                if(isEnoughLineSpace(lineBuilder, wordBuilder, maxLineLength)){
                    
                    lineBuilder.append(wordBuilder.toString());
                    wordBuilder = new StringBuilder();
                }
                else{
                    lineBuilder = fillUpWhitespaces(lineBuilder, maxLineLength);                   
                    textBuilder.append(lineBuilder.toString()+"\n");
                    lineBuilder = new StringBuilder();

                    lineBuilder.append(wordBuilder.toString());
                    wordBuilder = new StringBuilder();
                }
            }
            else{

                if(isOneWordLineSpace(wordBuilder, maxLineLength)){
                    lineBuilder = fillUpWhitespaces(lineBuilder, maxLineLength);
                    textBuilder.append(lineBuilder.toString()+"\n");
                    lineBuilder = new StringBuilder();     

                    lineBuilder.append(wordBuilder.toString());
                    wordBuilder = new StringBuilder();                      
                    wordBuilder.append(character);        
                }
                else{
                    wordBuilder.append(character);
                }

                if(character.equals('.')){
                    lineBuilder = fillUpWhitespaces(lineBuilder, maxLineLength);
                    textBuilder.append(lineBuilder.toString()+"\n");
                    lineBuilder = new StringBuilder();

                    lineBuilder.append(wordBuilder.toString());
                    wordBuilder = new StringBuilder();

                    lineBuilder = fillUpWhitespaces(lineBuilder, maxLineLength);
                    textBuilder.append(lineBuilder.toString());
                    lineBuilder = new StringBuilder();
                }
            }       
        }
		return textBuilder.toString();
    }

    public StringBuilder fillUpWhitespaces(StringBuilder lineBuilder, int maxLineLength){
        int lineLength = lineBuilder.length();
        if(lineLength < maxLineLength){
            StringBuilder whitespacesBuilder = new StringBuilder();
            while(lineLength<maxLineLength){
                whitespacesBuilder.append(" ");
                ++lineLength;
            }
            lineBuilder.append(whitespacesBuilder.toString());
        }
        return lineBuilder;
    }

    public boolean isEnoughLineSpace(StringBuilder lineBuilder, StringBuilder wordBuilder, int maxLineLength){
        return lineBuilder.length()+wordBuilder.length()<=maxLineLength;
    }

    public boolean isOneWordLineSpace(StringBuilder wordBuilder, int maxLineLength){
        return wordBuilder.length() >= maxLineLength;
    }
}
